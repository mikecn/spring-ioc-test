package com.lagou.edu.annotation;

import java.lang.annotation.*;

/**
 * @author lyj
 * @Title: Autowired
 * @ProjectName lagou-transfer
 * @Description: TODO
 * @date 2020/6/7 10:50
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME )
public @interface Autowired {
}
