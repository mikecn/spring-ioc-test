package com.lagou.edu.annotation;

import java.lang.annotation.*;

/**
 * @author lyj
 * @Title: Service
 * @ProjectName lagou-transfer
 * @Description: TODO
 * @date 2020/6/7 10:46
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME )
public @interface Service {
    public String value();
}
