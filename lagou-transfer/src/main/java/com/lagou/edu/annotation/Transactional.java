package com.lagou.edu.annotation;

import java.lang.annotation.*;

/**
 * @author lyj
 * @Title: Transactional
 * @ProjectName lagou-transfer
 * @Description: TODO
 * @date 2020/6/7 16:02
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME )
public @interface Transactional {
}
